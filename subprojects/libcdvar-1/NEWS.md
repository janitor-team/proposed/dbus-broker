# c-dvar - D-Bus Variant Type-System

## CHANGES WITH 1.0.0:

        * Initial release of c-dvar.

        Contributions from: Adrian Szyndela, David Rheinsberg, Evgeny
                            Vereshchagin, Tom Gundersen

        - Brno, 2022-06-22
