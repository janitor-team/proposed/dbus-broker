# c-shquote - POSIX Shell Compatible Argument Parser

## CHANGES WITH 1.0.0:

        * Initial release of c-shquote.

        Contributions from: David Rheinsberg, Evgeny Vereshchagin, Tom Gundersen

        - Brno, 2022-06-22
