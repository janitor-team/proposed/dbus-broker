# c-ini - Ini-File Handling

## CHANGES WITH 1.0.0:

        * Initial release of c-ini.

        Contributions from: David Rheinsberg, Evgeny Vereshchagin, Tom Gundersen

        - Brno, 2022-06-22
